from setuptools import setup

setup(name='grpc_messages',
        version='0.0.11',
        description='ehLabs gRPC messages',
        url='https://bitbucket.org/ehickox/messages',
        author='Eli Hickox',
        author_email='eli@elihickox.com',
        license='NCSA',
        install_requires=['grpcio==1.48.2', 'protobuf==4.21.12', 'six'],
        packages=['grpc_messages'])
