#!/usr/bin/env bash
python -m grpc_tools.protoc -I . --python_out=grpc_messages/. --grpc_python_out=grpc_messages/. *.proto
# sed command written for linux here. Add another '' before the '...' to get this to work on mac
sed -i 's/import messages_pb2 as messages__pb2/from grpc_messages import messages_pb2 as messages__pb2/g' grpc_messages/messages_pb2_grpc.py
protoc --go_out=plugins=grpc:. --go_opt=paths=source_relative *.proto

