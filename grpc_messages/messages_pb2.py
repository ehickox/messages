# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: messages.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0emessages.proto\x12\x08messages\"R\n\x0c\x45mailRequest\x12\x10\n\x08to_addrs\x18\x01 \x03(\t\x12\x11\n\tfrom_addr\x18\x02 \x01(\t\x12\x0f\n\x07subject\x18\x03 \x01(\t\x12\x0c\n\x04\x62ody\x18\x04 \x01(\t\"<\n\x14\x45mailRequestResponse\x12\x13\n\x0bstatus_code\x18\x01 \x01(\x05\x12\x0f\n\x07message\x18\x02 \x01(\t2N\n\x07\x45mailer\x12\x43\n\x07\x44oEmail\x12\x16.messages.EmailRequest\x1a\x1e.messages.EmailRequestResponse\"\x00\x42)Z\'bitbucket.org/ehickox/messages;messagesb\x06proto3')



_EMAILREQUEST = DESCRIPTOR.message_types_by_name['EmailRequest']
_EMAILREQUESTRESPONSE = DESCRIPTOR.message_types_by_name['EmailRequestResponse']
EmailRequest = _reflection.GeneratedProtocolMessageType('EmailRequest', (_message.Message,), {
  'DESCRIPTOR' : _EMAILREQUEST,
  '__module__' : 'messages_pb2'
  # @@protoc_insertion_point(class_scope:messages.EmailRequest)
  })
_sym_db.RegisterMessage(EmailRequest)

EmailRequestResponse = _reflection.GeneratedProtocolMessageType('EmailRequestResponse', (_message.Message,), {
  'DESCRIPTOR' : _EMAILREQUESTRESPONSE,
  '__module__' : 'messages_pb2'
  # @@protoc_insertion_point(class_scope:messages.EmailRequestResponse)
  })
_sym_db.RegisterMessage(EmailRequestResponse)

_EMAILER = DESCRIPTOR.services_by_name['Emailer']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Z\'bitbucket.org/ehickox/messages;messages'
  _EMAILREQUEST._serialized_start=28
  _EMAILREQUEST._serialized_end=110
  _EMAILREQUESTRESPONSE._serialized_start=112
  _EMAILREQUESTRESPONSE._serialized_end=172
  _EMAILER._serialized_start=174
  _EMAILER._serialized_end=252
# @@protoc_insertion_point(module_scope)
